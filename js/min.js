/*---------------------Menu--------------------------------*/
$(document).ready(main);
 
var contador = 1;
 
function main () {
	$('.menu_bar').click(function(){
		if (contador == 1) {
			$('nav').animate({
				left: '0'
			});
			contador = 0;
		} else {
			contador = 1;
			$('nav').animate({
				left: '-100%'
			});
		}
	});
 
	// Mostramos y ocultamos submenus
	$('.submenu').click(function(){
		$(this).children('.children').slideToggle();
	});
}

/*-------------------Rutas no disponibles-----------------------*/
function a(){
    alert("Lo sentimos esta ruta aún no esta disponible")
}
/*---------------------------------------------------------*/
/* Para cambiar de mapa*/ 


function p1(){
	 document.getElementById('mapa').src = "./images/central_p1.png";
}
function p0(){
	 document.getElementById('mapa').src = "./images/central_p0.png";
}
function patio(){
	 document.getElementById('mapa').src = "./images/patio.png";
}
function superior(){
	 document.getElementById('mapa').src = "./images/superior.png";
}
function medio(){
	 document.getElementById('mapa').src = "./images/medio.png";
}

function foto_audiovisual(){
	 document.getElementById('mapa').src = "./images/foto_audio.jpg";
}


function foto_f2a(){
	 document.getElementById('mapa').src = "./images/foto_f2a.jpg";
}


 /*---------------------------------------------------------*/
            /* Para mostrar las flechas*/

/*Estas pertenecen a las flechas del edificio central */
//Flechas de la biblioteca
function planta1() {
    'use strict';
    var x = document.getElementById("biblioteca_gif");
    //Flechas que van hacia arriba
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
    var y = document.getElementById("siguiente");
    //Flechas que van hacia arriba
    if (y.style.display === "block") {
        y.style.display = "none";
    } else {
        y.style.display = "block";
    }
}
//Esta funcion te enseña un boton para volver al mapa inicial
function anterior(){
	 'use strict';
	var anterior = document.getElementById("anterior");
    //Flechas que van hacia arriba
    if (anterior.style.display === "block") {
        anterior.style.display = "none";
    } else {
        anterior.style.display = "block";
    }
}

//Esta funcion te enseña un link para ver la biblioteca
function ver_biblioteca(){
	 'use strict';
	var foto = document.getElementById("foto_biblio");
    //Flechas que van hacia arriba
    if (foto.style.display === "block") {
        foto.style.display = "none";
    } else {
        foto.style.display = "block";
    }
}





//Flechas de direccion
//Para ver las flechas a direccion
function direccion() {
      'use strict';
    var x = document.getElementById("biblioteca_gif");
    //Flechas que van hacia arriba
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
    var y = document.getElementById("siguiente_direccion");
    //Flechas que van hacia arriba
    if (y.style.display === "block") {
        y.style.display = "none";
    } else {
        y.style.display = "block";
    }  
}

//Esta funcion te enseña un boton para volver al mapa inicial
function anterior_direccion(){
	 'use strict';
	var anterior = document.getElementById("anterior_direccion");
    //Flechas que van hacia arriba
    if (anterior.style.display === "block") {
        anterior.style.display = "none";
    } else {
        anterior.style.display = "block";
    }
}

//Esta funcion te enseña un link para ver la direccion
function ver_direccion(){
	 'use strict';
	var foto = document.getElementById("foto_direccion");
    //Flechas que van hacia arriba
    if (foto.style.display === "block") {
        foto.style.display = "none";
    } else {
        foto.style.display = "block";
    }
}


//####################Audiovisuales-Ruta##########################
function fusteria() {
     
    var fusteria = document.getElementById("abajo_central");
    
    if (fusteria.style.display === "block") {
        fusteria.style.display = "none";
    } else {
        fusteria.style.display = "block";
    }
    
   var fixed = document.getElementById("audio_central");
    
    if (fixed.style.display === "block") {
        fixed.style.display = "none";
    } else {
        fixed.style.display = "block";
    }
    
}


//##############################Mapa-Patio################################
function sorpresa() {
    'use strict';
    var izquierda_rara = document.getElementById("rara_patio");
       
    
    if (izquierda_rara.style.display === "block") {
        izquierda_rara.style.display = "none";
    } else {
        izquierda_rara.style.display = "block";
    }
    
}
function anterior_audio(){
	 'use strict';
	var anterior = document.getElementById("anterior_audio");
    //Flechas que van hacia arriba
    if (anterior.style.display === "block") {
        anterior.style.display = "none";
    } else {
        anterior.style.display = "block";
    }
}
//########################Boton-Final###########################

//Para volver despues de la foto

function anterior_patio(){
	 'use strict';
	var foto = document.getElementById("anterior_patio");
    //Flechas que van hacia arriba
    if (foto.style.display === "block") {
        foto.style.display = "none";
    } else {
        foto.style.display = "block";
    }
}

function ver_audiovisual(){
	 'use strict';
	var audio = document.getElementById("foto_audiovisual");
    //Flechas que van hacia arriba
    if (audio.style.display === "block") {
        audio.style.display = "none";
    } else {
        audio.style.display = "block";
    }
}



//###############Ruta-F2A##################################
function f2a() {
     
    var fusteria = document.getElementById("abajo_central");
    
    if (fusteria.style.display === "block") {
        fusteria.style.display = "none";
    } else {
        fusteria.style.display = "block";
    }
    
   var fixed = document.getElementById("siguiente_gmedio");
    
    if (fixed.style.display === "block") {
        fixed.style.display = "none";
    } else {
        fixed.style.display = "block";
    }
    
}
//###################Ruta-Patio################################
function fusta_p0() {
    'use strict';
    var izquierda_rara = document.getElementById("rara_patio");
       
    
    if (izquierda_rara.style.display === "block") {
        izquierda_rara.style.display = "none";
    } else {
        izquierda_rara.style.display = "block";
    }
    var dep_info = document.getElementById("siguiente_gmediop2");
    
    if (dep_info.style.display === "block") {
        dep_info.style.display = "none";
    } else {
        dep_info.style.display = "block";
    }
}

function anterior_patio_f2a() {
    'use strict';
    var anterior_patio = document.getElementById("anterior_patio_f2a");
       
    
    if (anterior_patio.style.display === "block") {
        anterior_patio.style.display = "none";
    } else {
        anterior_patio.style.display = "block";
    }
    
}
//########################Boton-Final###########################
function fusta_p1(){
	var f2a = document.getElementById("f2a");
    
    if (f2a.style.display === "block") {
        f2a.style.display = "none";
    } else {
        f2a.style.display = "block";
    }
    
    var anterior = document.getElementById("anterior_gmedio");
    
    if (anterior.style.display === "block") {
        anterior.style.display = "none";
    } else {
        anterior.style.display = "block";
    }
}

function anterior_fotof2a(){
	 var anterior_fotof2a = document.getElementById("anterior_fotof2a");
    
    if (anterior_fotof2a.style.display === "block") {
        anterior_fotof2a.style.display = "none";
    } else {
        anterior_fotof2a.style.display = "block";
    }
}